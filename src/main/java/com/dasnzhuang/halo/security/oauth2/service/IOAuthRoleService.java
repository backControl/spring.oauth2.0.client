package com.dasnzhuang.halo.security.oauth2.service;

public interface IOAuthRoleService {

    public String loadFunctionTree(Long roleId);
}
