package com.dasnzhuang.halo.security.oauth2.service.impl;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;

import com.dasnzhuang.halo.security.oauth2.service.IOAuthRoleService;

@Service
public class OAuthRoleServiceImpl implements IOAuthRoleService {

    @Autowired
    @Qualifier("roleRestTemplate")
    RestOperations roleRestTemplate;

    @Value("${halo.frame.variables.role.loadfunction.url}")
    private String roleLoadFunctionTreeUrl;

    @Override
    public String loadFunctionTree(Long roleId) {
        URI uri = URI.create(String.format(roleLoadFunctionTreeUrl, roleId));
        return this.roleRestTemplate.getForObject(uri, String.class);
    }
}
