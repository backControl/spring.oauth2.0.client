package com.dasnzhuang.halo.security.oauth2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.dasnzhuang.halo.security.oauth2.service.IOAuthRoleService;

@Controller
@RequestMapping("/oauthrole")
public class OAuthRoleController {

    @Autowired
    private IOAuthRoleService roleService;

    @RequestMapping("/loadFunctionTree")
    @ResponseBody
    public Object function(Long id, Model model) {
        return JSON.parse(roleService.loadFunctionTree(id));
    }
}
