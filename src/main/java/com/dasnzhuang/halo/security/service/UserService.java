package com.dasnzhuang.halo.security.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dasnzhuang.halo.security.entity.SysUser;

@Repository
public interface UserService extends JpaRepository<SysUser, Integer> {

    public SysUser findByName(String userName);

}
