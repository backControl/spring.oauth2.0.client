package com.dasnzhuang.halo.security.start;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.dasnzhuang.halo.security.entity.SysUser;
import com.dasnzhuang.halo.security.service.UserService;

@Component
public class MyStartupRunner1 implements CommandLineRunner {
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {
        System.out.println(">>>>>>>>>>>>>>>服务启动执行，执行加载数据等操作<<<<<<<<<<<<<");
        userService.deleteAll();
        SysUser sysUser = new SysUser();
        sysUser.setName("admin");
        sysUser.setPassword(passwordEncoder.encode("admin"));
        sysUser.setEmail("admin@speedbuy.online");
        userService.save(sysUser);
    }

}
